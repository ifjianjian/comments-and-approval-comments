# 评论审批意见

#### 介绍
1、传统jsp项目使用vue改
2、实现@人的审批意见或评论
3、@人删除时候，整个删除

####待解决问题
1、IE模式下，删除时，不能整个删除
2、IE模式下，光标定位删除有问题

#### 效果
![取值](img%E5%8F%96%E5%80%BC.png)

#### 软件架构
软件架构说明
1、可npm或者传统jsp
2、利用wangeditor
3、git例子，是用传统jsp实现的vue

#### 安装教程
传统项目，下载即用
#npm项目
1.  npm i wangeditor --save
2.  npm install --save @babel/polyfill

#### 使用说明

1.  参考文案（此文案是用vue）：https://jishuin.proginn.com/p/763bfbd701e4

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
