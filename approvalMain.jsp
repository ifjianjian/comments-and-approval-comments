<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>

<style lang="scss" scoped>
	::v-deep .w-e-text-container {
	    min-height: 100px;
	    max-height: 300px;
	    height: auto !important;
	    border: 1px solid #dbdbdb !important;
	    border-radius: 4px;
	    overflow-y: auto;
	}
</style>

<!-- Vue.js v2.6.12 -->
<script src="js/vue.js"></script>
<script src="js/wangEditor.min.js"></script>
<script src="js/babel.min.js"></script>
<script src="js/polyfill.min.js"></script>

<div id="approval-main" >
	<div 
		ref="editor"
		@compositionstart="compositionstart"
        @compositionend="compositionend"
        @click="onClickEditor"
        @keydown="onKeyDownInput($event)"
	></div>
	
	<a  href="javascript:void(0);" @click="getApprovalContent">取值</a>
</div>

<script type="text/babel">
var app = new Vue({
	el:'#approval-main',
    data() {
        return {
            editor: '',
            isChineseInputMethod: false, // 是否中文输入法状态中
        	position: ''
        }
    },
    mounted() {
        this.initEditor() // 初始化编辑器
    },
    methods: {
        	initEditor() {
        		var E = window.wangEditor
	            let editor = new E(this.$refs.editor)
	            editor.config.placeholder = '写审批意见~可手动输入@通知其他人'
	            editor.config.menus = [] // 显示菜单按钮
	            editor.config.showFullScreen = false // 不显示全屏按钮
	            editor.config.pasteIgnoreImg = true // 如果复制的内容有图片又有文字，则只粘贴文字，不粘贴图片。
	            editor.config.height = '100'
	            editor.config.focus = false  // 取消自动 focus
	            editor.create()
	            this.editor = editor
	            // 销毁编辑器，定义与销毁应该在同一个地方，增加阅读性，方便后期维护。
	            this.$once('hook:beforeDestroy', () => { 
	                this.editor.destroy()
	                this.editor = null
	            })
	            // 编辑的文本的时候记录光标。
	            editor.config.onchange = html => {
	                // 生成@的标签的时候会触发渲染、此时不要记录光标坐标
	                if (this.isRendering == false) {
	                    this.setRecordCoordinates() // 记录坐标
	                }
	            }
	        },
	        
	        // 中文输入触发
	        compositionstart() {
	            this.isChineseInputMethod = true
	        },
	
	        // 中文输入关闭
	        compositionend() {
	            this.isChineseInputMethod = false
	        },
	        // 每次点击获取更新坐标
	        onClickEditor() {
	            this.setRecordCoordinates()
	        },
	        
	        // keydown触发事件 记录光标
	        onKeyDownInput(e) {
	            // @的键盘时间判断
			    const isCode = ((e.keyCode === 229 && e.key === '@') || (e.keyCode === 229 && e.code === 'Digit2') || e.keyCode === 50) && e.shiftKey
			    // 判断状态是否不是中文输入法，并且监听到了@的事件
			    if (!this.isChineseInputMethod && isCode) {
			    	var _that = this;
			    	// 模拟加载列表时的延迟，此处延迟还可以防止方法触发的比@早
			        setTimeout(function(){
				        // 记录当前文本光标坐标位置
				        _that.setRecordCoordinates(); // 保存坐标
				        // 打开弹窗的方法xxxx，这里就省略了
				        // 调用赋值
			       		_that.createSelectElement('小明','123','default');
			        },100);
			    }
	        },
	        
	        // 获取当前光标坐标
	        setRecordCoordinates() {
	            try {
	                // getSelection() 返回一个 Selection 对象，表示用户选择的文本范围或光标的当前位置。
	                const selection = getSelection()
	                this.position = {
	                    range: selection.getRangeAt(0),
	                    selection: selection
	                }
	            } catch (error) {
	                console.log(error, '光标获取失败了～')
	            }
	        },
	        /**
			* 数据结构：
			* userList: [{name: '坏女人', uid: 18}, {name: '好男人', uid: 888}]
			*/
			
			//弹窗列表 - 选人 - 生成@的内容
			createSelectElement(name, id, type = 'default') {
			    // 获取当前文本光标的位置。
			    const { selection, range } = this.position
			    // 生成需要显示的内容
			    let spanNodeFirst = document.createElement('span')
			    spanNodeFirst.style.color = '#409EFF'
			    spanNodeFirst.innerHTML = '@'+name+'&nbsp;' // @的文本信息
			    spanNodeFirst.dataset.id = id // 用户ID、为后续解析富文本提供
			    spanNodeFirst.contentEditable = false // 当设置为false时，富文本会把成功文本视为一个节点。
			    
			    // 需要在字符前插入一个空格否则、在换行与两个@标签连续的时候导致无法删除标签
			    let spanNode = document.createElement('span');
			    spanNode.innerHTML = '&nbsp;';
			
			    //创建一个新的空白的文档片段，拆入对应文本内容
			    let frag = document.createDocumentFragment()
			    frag.appendChild(spanNode);
			    frag.appendChild(spanNodeFirst);
			
			    // 是否需要删除键盘触发的默认@，
			    if (type === 'default') {
			        const textNode = range.startContainer;
			        range.setStart(textNode, range.endOffset - 1);
			        range.setEnd(textNode, range.endOffset);
			        range.deleteContents();
			    }
			    
			    // 判断是否有文本、是否有坐标
			    if ((this.editor.txt.text() || type === 'default')&& this.position && range) {
			        range.insertNode(frag)
			    } else {
			        // 如果没有内容一开始就插入数据特别处理
			        this.editor.txt.append('<span data-id='+id+' style="color: #409EFF" contentEditable="false">@'+name+'&nbsp;</span>')
			    }
			},
			
	        /**
			* 例 富文本： <p><span data-id="idxxx">@小明</span> <br/> 喂三点几拉 <span data-id="idxxx">@饮茶哥</span> 出来饮茶
			* 生成数组： 
			* [
			*    {segment: '@小明 \n', userId: 'idxxxxx'},
			*    {segment: '喂三点几拉', userId: null},
			*    {segment: '@饮茶哥', userId: 'idxxxx'},
			*    {segment: '出来饮茶', userId: null}
			* ]
			*/
			
			// 解析编辑器富文本 生成文本信息数组
			fetchGenerateContentsArray() {
			    // 获取编辑器的JSON对象
			    const data = this.editor.txt.getJSON()
			    
			    let contents = []
			    // 解析html列表JSON，生成文本对象。
			    const generateArray = nodeList => {
			        if (Array.isArray(nodeList)) {
			            nodeList.forEach(item => {
			                // 对于换行符号处理 处理<br> <p>等标签保障显示一致性
			                if (item && item.tag) {
			                    // 针对富文本列表是特殊换行处理 {tag:p, children: [{tag: 'br'}]} 这件换行过滤
			                    const notSpecialLabel = item.tag == 'p' && item.children[0] && item.children[0].tag == 'br'
			                    if (!notSpecialLabel && ['p', 'br'].includes(item.tag) && contents.length) {
			                        const index = contents.length - 1
			                        // 在文本中拆入换行符号兼容android、ios的换行字符
			                        contents[index].segment += '\n' 
			                    }
			                }
			                
			                //  如果遍历的属性是 data-id 有ID的
			                if (item && item.attrs && item.attrs.find(e => e.name === 'data-id')) {
			                    const id = item.attrs.find(e => e.name === 'data-id').value
			                    const content = item.children && item.children[0] || ''
			                    contents.push({ segment: content.replace(/(<br>)|(<br\/>)/g, ''), userId: id })
			                    return
			                }
			                
			                // 如果children 是数组则继续递归遍历下一层
			                if (Array.isArray(item.children)) {
			                    generateArray(item.children)
			                    return
			                }
			                
			                // 如果没有数组了、就是文本的内容
			                // 删除文本中的 <br> 字符
			                if (item.trim()) {
			                    contents.push({segment: item.replace(/(<br>)|(<br\/>)/g, ''), userId: ''})
			                }
			            })
			        }
			    }
			    generateArray(data);
			    return contents
			},
			// 获取富文本框内容
			getApprovalContent() {
				var segments = [];
				var userIds = [];
				var userNames = [];
			    var contents = this.fetchGenerateContentsArray();
			    for(var c = 0; c < contents.length; c++){
			    	contents[c].segment && segments.push(contents[c].segment);
			    	contents[c].userId && userIds.push(contents[c].userId);
			    	contents[c].userId && userNames.push(contents[c].segment);
			    }
			    console.log(segments.join(''));
			    console.log(userIds.join(','));
			    console.log(userNames.join(','));
			}
			
    } 
});
</script>